﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

namespace VeryAnimation
{
    [Serializable]
    public class BlendShapeTree
    {
        private VeryAnimationWindow vaw { get { return VeryAnimationWindow.instance; } }
        private VeryAnimation va { get { return VeryAnimation.instance; } }
        private VeryAnimationEditorWindow vae { get { return VeryAnimationEditorWindow.instance; } }

        [System.Diagnostics.DebuggerDisplay("{blendShapeName}")]
        private class BlendShapeInfo
        {
            public string blendShapeName;
        }
        private class BlendShapeNode
        {
            public string name;
            public bool foldout;
            public BlendShapeInfo[] infoList;
        }
        private class BlendShapeRootNode : BlendShapeNode
        {
            public SkinnedMeshRenderer renderer;
            public Mesh mesh;
        }
        private List<BlendShapeRootNode> blendShapeNodes;

        public BlendShapeTree()
        {
            if (vaw == null || va.editGameObject == null)
                return;

            #region BlendShapeNode
            {
                blendShapeNodes = new List<BlendShapeRootNode>();
                foreach (var renderer in va.editGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    if (renderer.sharedMesh == null) continue;
                    if (renderer.sharedMesh.blendShapeCount <= 0) continue;
                    BlendShapeRootNode root = new BlendShapeRootNode()
                    {
                        renderer = renderer,
                        mesh = renderer.sharedMesh,
                        name = renderer.gameObject.name,
                        infoList = new BlendShapeInfo[renderer.sharedMesh.blendShapeCount],
                    };
                    for (int i = 0; i < renderer.sharedMesh.blendShapeCount; i++)
                    {
                        root.infoList[i] = new BlendShapeInfo()
                        {
                            blendShapeName = renderer.sharedMesh.GetBlendShapeName(i),
                        };
                    }
                    blendShapeNodes.Add(root);
                }
            }
            #endregion
        }
        
        public bool IsHaveBlendShapeNodes()
        {
            return blendShapeNodes.Count > 0;
        }

        public void BlendShapeTreeGUI()
        {
            var e = Event.current;

            EditorGUILayout.BeginVertical(GUI.skin.box);
            {
                float LabelWidth = Mathf.Min(VeryAnimationEditorWindow.instance.position.width / 2f, 400f);
                const int IndentWidth = 15;

                #region GetBlendShapeLevel
                Func<BlendShapeNode, int, int> GetBlendShapeLevel = null;
                GetBlendShapeLevel = (mg, level) =>
                {
                    if (mg.foldout)
                    {
                        if (mg.infoList != null && mg.infoList.Length > 0)
                        {
                            level++;
                        }
                    }
                    return level;
                };
                #endregion
                #region SetBlendShapeFoldout
                Action<BlendShapeNode, bool> SetBlendShapeFoldout = null;
                SetBlendShapeFoldout = (mg, foldout) =>
                {
                    mg.foldout = foldout;
                };
                #endregion

                var mgRoot = blendShapeNodes;

                #region Reset All
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.Space();
                    if (GUILayout.Button("Reset All", GUILayout.Width(100)))
                    {
                        Undo.RecordObject(vae, "Reset All BlendShape Group");
                        foreach (var root in mgRoot)
                        {
                            if (root.infoList != null && root.infoList.Length > 0)
                            {
                                foreach (var info in root.infoList)
                                {
                                    if (va.IsHaveAnimationCurveBlendShape(root.renderer, info.blendShapeName) || va.blendShapeWeightSave.GetOriginalWeight(root.renderer, info.blendShapeName) != 0f)
                                        va.SetAnimationCurveBlendShape(root.renderer, info.blendShapeName, 0f);
                                }
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
                #endregion

                EditorGUILayout.Space();

                #region BlendShape
                BlendShapeRootNode rootNode = null;
                int RowCount = 0;
                Action<BlendShapeNode> BlendShapeTreeGUI = null;
                BlendShapeTreeGUI = (mg) =>
                {
                    EditorGUILayout.BeginHorizontal(RowCount++ % 2 == 0 ? vaw.guiStyleAnimationRowEvenStyle : vaw.guiStyleAnimationRowOddStyle);
                    EditorGUI.BeginChangeCheck();
                    mg.foldout = EditorGUILayout.Foldout(mg.foldout, new GUIContent(mg.name), true);
                    if (EditorGUI.EndChangeCheck())
                    {
                        if (e.alt)
                            SetBlendShapeFoldout(mg, mg.foldout);
                    }
                    if (mg == rootNode)
                    {
                        EditorGUI.BeginDisabledGroup(true);
                        EditorGUILayout.ObjectField(rootNode.renderer, typeof(SkinnedMeshRenderer), false, GUILayout.Width(128f));
                        EditorGUI.EndDisabledGroup();
                    }
                    else
                    {
                        EditorGUILayout.Space();
                    }
                    GUILayout.Space(IndentWidth * GetBlendShapeLevel(mg, 0));
                    if (GUILayout.Button("Reset", GUILayout.Width(44)))
                    {
                        Undo.RecordObject(vae, "Reset BlendShape Group");
                        if (mg.infoList != null && mg.infoList.Length > 0)
                        {
                            foreach (var info in mg.infoList)
                            {
                                if (va.IsHaveAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName) || va.blendShapeWeightSave.GetOriginalWeight(rootNode.renderer, info.blendShapeName) != 0f)
                                    va.SetAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName, 0f);
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    if (mg.foldout)
                    {
                        EditorGUI.indentLevel++;
                        if (mg.infoList != null && mg.infoList.Length > 0)
                        {
                            #region BlendShape
                            foreach (var info in mg.infoList)
                            {
                                var blendShapeValue = va.GetAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName);
                                EditorGUILayout.BeginHorizontal(RowCount++ % 2 == 0 ? vaw.guiStyleAnimationRowEvenStyle : vaw.guiStyleAnimationRowOddStyle);
                                EditorGUI.indentLevel++;
                                var rect = GUILayoutUtility.GetRect(new GUIContent(info.blendShapeName), GUI.skin.label, GUILayout.Width(LabelWidth), GUILayout.Height(22));
                                {
                                    rect.x += IndentWidth * EditorGUI.indentLevel;
                                    rect.width -= IndentWidth * EditorGUI.indentLevel;
                                    rect.height -= 4;
                                }
                                if (GUI.Button(rect, new GUIContent(info.blendShapeName)))
                                {
                                    va.SetAnimationWindowSynchroSelection(new EditorCurveBinding[] { va.AnimationCurveBindingBlendShape(rootNode.renderer, info.blendShapeName) });
                                }
                                GUILayoutUtility.GetRect(0f, 0f);
                                {
                                    EditorGUI.BeginChangeCheck();
                                    var value2 = GUILayout.HorizontalSlider(blendShapeValue, 0f, 100f, GUILayout.Width(vaw.editorSettings.settingEditorSliderSize));
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        va.SetAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName, value2);
                                    }
                                }
                                if (GUILayout.Button("Reset", GUILayout.Width(44)))
                                {
                                    if (va.IsHaveAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName) || va.blendShapeWeightSave.GetOriginalWeight(rootNode.renderer, info.blendShapeName) != 0f)
                                        va.SetAnimationCurveBlendShape(rootNode.renderer, info.blendShapeName, 0f);
                                }
                                EditorGUI.indentLevel--;
                                EditorGUILayout.EndHorizontal();
                            }
                            #endregion
                        }
                        EditorGUI.indentLevel--;
                    }
                };
                foreach (var root in mgRoot)
                {
                    if (root.renderer != null && root.mesh != null && root.renderer.sharedMesh == root.mesh)
                    {
                        rootNode = root;
                        BlendShapeTreeGUI(root);
                    }
                }
                #endregion
            }
            EditorGUILayout.EndVertical();
        }
    }
}
