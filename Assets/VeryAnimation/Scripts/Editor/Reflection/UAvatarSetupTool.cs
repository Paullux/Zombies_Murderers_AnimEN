﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Reflection;

namespace VeryAnimation
{
    public class UAvatarSetupTool
    {
        private MethodInfo mi_SampleBindPose;

        public UAvatarSetupTool()
        {
            var asmUnityEditor = Assembly.LoadFrom(InternalEditorUtility.GetEditorAssemblyPath());
            var avatarSetupToolType = asmUnityEditor.GetType("UnityEditor.AvatarSetupTool");
            Assert.IsNotNull(mi_SampleBindPose = avatarSetupToolType.GetMethod("SampleBindPose", BindingFlags.Public | BindingFlags.Static));
        }

        public bool SampleBindPose(GameObject go)
        {
            foreach (var renderer in go.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                if (renderer.sharedMesh == null)
                    return false;
                else if (renderer.bones.Length != renderer.sharedMesh.bindposes.Length)
                {
                    Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The count of bones in SkinnedMeshRenderer and bindposes count of Mesh do not match. '{0}' != '{1}'", renderer.name, renderer.sharedMesh.name);
                    return false;
                }
            }
            mi_SampleBindPose.Invoke(null, new object[] { go });
            return true;
        }
    }
}
