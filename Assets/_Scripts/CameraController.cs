﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    public bool Zoom = false;

    public Transform target;
    public Transform mouse;

    private Vector3 offset;

    public float speed;

    void Update()
    {
        float step = speed * Time.deltaTime;
        if (!Zoom) transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        if (transform.position == target.position)
        {
            Zoom = true;
            offset = transform.position - mouse.transform.position;
            mouse.GetComponent<WanderScript>().enabled = true;
        }
    }
    void LateUpdate()
    {
        if (Zoom) transform.position = mouse.transform.position + offset;
    }
}
