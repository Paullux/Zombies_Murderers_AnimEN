﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change2Zombies : MonoBehaviour {
    public GameObject Etat1, Etat2, Etat3;
    public float Changement1 = 15, Changement2 = 18;
    // Use this for initialization
	void Start ()
    {
        Etat2.SetActive(false);
        Etat1.SetActive(true);
        Etat3.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.timeSinceLevelLoad >= Changement1 && Time.deltaTime <= Changement2)
        {
            Etat1.SetActive(false);
            Etat2.SetActive(true);
            Etat3.SetActive(false);
        }
        if (Time.timeSinceLevelLoad > Changement2)
        {
            Etat1.SetActive(false);
            Etat2.SetActive(false);
            Etat3.SetActive(true);
        }
    }
}
